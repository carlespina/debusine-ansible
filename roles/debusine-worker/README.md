# debusine-worker role

Variables:
debusine_server_url: specify the debusine server API URL

The role installs debusine-worker distribution:

* Download debusine's salsa generated Debian packages from the latest "build" artifact from salsa
* Install debusine's worker required packages
* Add debusine-worker user to the group sbuild
* Use sbuild-createchroot to create the chroots

At the end of this: the debusine-admin could execute in the debusine-server:
``
sudo -u debusine-server debusine-admin list_workers
``
And see the new registered worker (which needs to be enabled).
