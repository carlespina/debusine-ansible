# debusine-server role

The role installs debusine-server in a Debian 11 (bullseye) distribution:

* Add bullseye/backports to install the required Django version
* Download debusine's salsa generated Debian packages from the latest "build" artifact from salsa
* Install postgres and redis packages
* Install debusine server required packages
* Set up postgres user for debusine, run the initial migrations
* Set up nginx and debusine's ALLOWED_HOSTS to allow connections via the server's IP
